#!/usr/bin/env bash
#assumes that xcode-select --install was run

printf '\nInstalling HomeBrew packages...\n' >&2
brew bundle install --file='manual-backups/Brewfile'

# Vundle
printf '\nChecking Vundle...\n' >&2
if ! [[ -d "$HOME/.vim/bundle/Vundle.vim" ]]; then
  git clone 'https://github.com/VundleVim/Vundle.vim.git' ~/.vim/bundle/Vundle.vim
fi

printf '\nInstalling Vundle packages...\n'
while true; do
  # keep trying until it works
  vim +PluginInstall +qall && break
done

# rust and cargo
printf '\nChecking Cargo...\n' >&2
if ! cargo --version; then
  rustup-init --no-modify-path -y
fi

while read PACKAGE; do
  cargo install "$PACKAGE"
done < 'manual-backups/cargo_packages.txt'
