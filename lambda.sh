#!/usr/bin/env bash

echo ~/.bash_profile
echo ~/.bash_profile.core
echo ~/.config/alacritty/alacritty.yml
echo ~/.config/htop/htoprc
echo ~/.config/neofetch/config.conf
echo ~/.gitconfig
echo ~/.gnupg/gpg-agent.conf
echo ~/.tmux.conf
echo ~/.tmux.conf.personal
echo ~/.tmux.conf.rustychoi
echo ~/.vimrc
echo ~/run.sh
