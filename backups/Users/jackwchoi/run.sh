#!/usr/bin/env bash

# parse options and args
declare -r SHORT_OPS='crsu'
declare -r LONG_OPS='clean,restart,shutdown,update'
declare -r OPTS=$(getopt --unquoted --options $SHORT_OPS --longoptions $LONG_OPS -- "$@") PARSED=$?

# exit if parsing failed
[[ $PARSED != 0 ]] && exit 1

# set as $@
set -- $OPTS

CLEAN=false
RESTART=false
SHUTDOWN=false
UPDATE=false
while true; do
  case $1 in
    -c | --clean )        CLEAN=true;        shift;;
    -r | --restart )      RESTART=true;      shift;;
    -s | --shutdown )     SHUTDOWN=true;     shift;;
    -u | --update )       UPDATE=true;       shift;;
    * )  # specifically "--"
      shift; break;;
  esac
done

# echo with one \n prepended, and in red
function necho {
  echo -e "\n\e[31m$1\e[0m" >&2
}

# rewrite this using rust regexset
function clean_quick {
  #
  : | pbcopy

  #
  necho 'Emptying trash...'
  empty-trash &

  #
  necho 'Cleaning files...'
  {
    find ./ -maxdepth 2 -type f -name '.*'
    find ~/ -maxdepth 2 -type f -name '.*'
    find ~/{Desktop,Documents,Downloads}/ -maxdepth 2 -type f -name '.*'
  } |
    rg '/.(bash|python|zsh)_(history|sessions)|(viminfo|DS_Store|history|aspell\.en\.(prepl|pws)|wget-hsts|lesshst|swp)$' |
    sort | uniq |
    rmf -t

  qlmanage -r

  : | pbcopy
  wait
}

function update {
  necho 'Updating HomeBrew...'
  arch -arm64 brew update
  necho 'Upgrading HomeBrew...'
  arch -arm64 brew upgrade

  necho 'Updating Rust...'
  rustup update
  rustup component add rustfmt
  cargo install-update --all

  necho 'Updating Vim...'
  vim +PluginInstall +qall
  vim +PluginUpdate +qall
  vim +PluginInstall +qall
}

if $CLEAN; then
  necho 'Performing QUICK CLEAN...'
  clean_quick
fi

if $UPDATE; then
  necho 'Performing UPDATE...'
  update
fi

if $SHUTDOWN; then
  necho 'Performing SHUTDOWN...'
  shutdown
elif $RESTART; then
  necho 'Performing RESTART...'
  restart
fi
