# paths
export PATH="$(getconf PATH)"  # prevents `r` from duplicating $PATH

#
export PATH="$HOME/.local/bin:$PATH"
export PATH="/Library/TeX/texbin:$PATH"
export PATH="/opt/homebrew/Caskroom/miniconda/base/bin:$PATH"
export PATH="/opt/homebrew/Cellar/gnu-which/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/bin:$PATH"
export PATH="/opt/homebrew/opt/coreutils/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/findutils/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/gnu-getopt/bin:$PATH"
export PATH="/opt/homebrew/opt/gnu-sed/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/gnu-tar/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/gnu-time/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/grep/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/make/libexec/gnubin:$PATH"
export PATH="/opt/homebrew/opt/ncurses/bin:$PATH"
export PATH="/opt/homebrew/opt/openssl/bin:$PATH"
export PATH="/opt/homebrew/opt/scala/libexec/bin:$PATH"
export PATH="/opt/homebrew/opt/unzip/bin:$PATH"
export PATH="/opt/homebrew/opt/zip/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"  # for clamd
# personal
export PATH="$HOME/.cargo/bin:$PATH"  # for clamd
#
export WORKSPACE="$HOME/tresor-jack"
export PATH="$WORKSPACE/execs:$PATH"

# for `man` to work with gnu utils
export MANPATH="/opt/homebrew/opt/coreutils/libexec/gnuman:$MANPATH"
export MANPATH="/opt/homebrew/opt/findutils/libexec/gnuman:$MANPATH"
export MANPATH="/opt/homebrew/opt/make/libexec/gnuman:$MANPATH"

################################ core

#
source "$HOME/.bash_profile.core"

################################ misc

# shell options
shopt -s autocd cdspell globstar gnu_errfmt

#
neofetch

#
#cal -3

# starship
export STARSHIP_CONFIG="$HOME/.config/starship.toml"
#
#eval "$(starship init bash)"

printf "\e[6 q"
