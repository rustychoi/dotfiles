set nocompatible

" vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
    Plugin 'VundleVim/Vundle.vim'
    Plugin 'airblade/vim-gitgutter'
    Plugin 'junegunn/fzf'
    Plugin 'junegunn/fzf.vim'
    Plugin 'neovimhaskell/haskell-vim'
    Plugin 'octol/vim-cpp-enhanced-highlight'
    Plugin 'reedes/vim-pencil'
    Plugin 'rust-lang/rust.vim'
    Plugin 'sonph/onehalf', {'rtp': 'vim/'}
    Plugin 'townk/vim-autoclose'
    Plugin 'tpope/vim-markdown'
    Plugin 'vim-airline/vim-airline'
    Plugin 'vim-airline/vim-airline-themes'
    Plugin 'vim-python/python-syntax'
    Plugin 'vim-scripts/a.vim'
call vundle#end()

"
set formatoptions+=ro

"
"let g:indentLine_setConceal = 0

" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline_theme = 'bubblegum'

" cpp-enhanced-highlight
let g:cpp_class_decl_highlight = 1
let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_posix_standard = 1

" python
let g:python_version_2 = 0
let g:python_highlight_all = 1
let g:python_highlight_builtin_funcs = 1
let g:python_highlight_builtin_funcs_kwarg = 1
let g:python_highlight_builtin_objs = 1
let g:python_highlight_builtin_types = 1
let g:python_highlight_builtins = 1
let g:python_highlight_class_vars = 1
let g:python_highlight_doctests = 1
let g:python_highlight_exceptions = 1
let g:python_highlight_file_headers_as_comments = 1
let g:python_highlight_func_calls = 1
let g:python_highlight_indent_errors = 1
let g:python_highlight_operators = 1
let g:python_highlight_space_errors = 1
let g:python_highlight_string_format = 1
let g:python_highlight_string_formatting = 1
let g:python_highlight_string_templates = 1

" syntastic
" let g:syntastic_check_on_wq = 0
"let g:syntastic_cpp_auto_refresh_includes = 1
"let g:syntastic_cpp_check_header = 0
"let g:syntastic_cpp_compiler = 'g++'
"let g:syntastic_cpp_compiler_options = ' -std=c++17 -Wall -Wextra -Wpedantic -O0'
"let g:syntastic_cpp_no_default_include_dirs = 1
"let g:syntastic_cpp_no_include_search = 1

" clang_complete
"let g:clang_auto_select=1
"let g:clang_close_preview=1
"let g:clang_complete_auto = 1
"let g:clang_complete_macros=1
"let g:clang_complete_patterns=1
"let g:clang_library_path='/Library/Developer/CommandLineTools/usr/lib/'
"let g:clang_snippets=1
"let g:clang_trailing_placeholder=1
"let g:clang_user_options = '-std=c++17 -Wall -O0'

" indentLine
let g:indentLine_char = '┊'

" mucomplete
"set infercase
"set completeopt=""
"set completeopt+=menu,noinsert
"set completeopt-=longest,menunuopt,preview,popup,noselect,menuone,popuphidden
"set shortmess+=c
"let g:mucomplete#completion_delay = 1
"let g:mucomplete#enable_auto_at_startup = 1

"""""""""""""""""""""""""""""""" builtin configs

syntax on

set background=dark

colorscheme onehalfdark
let g:airline_theme='onehalfdark'
highlight comment ctermbg=None ctermfg=darkgray cterm=None

" use the default background color of the terminal
highlight LineNr ctermbg=None ctermfg=None
highlight Normal ctermbg=None ctermfg=None

" show added/changed/deleted lines using git
highlight GitGutterAdd    ctermbg=None ctermfg=green
highlight GitGutterChange ctermbg=None ctermfg=yellow
highlight GitGutterDelete ctermbg=None ctermfg=red

" configs for the current line/column
" set cursorcolumn
set cursorline
set number relativenumber
" highlight CursorColumn ctermbg=None ctermfg=None cterm=underline
highlight CursorLine   ctermbg=None ctermfg=None cterm=underline
highlight CursorLinenr ctermfg=blue

" search highlights
highlight Search ctermfg=black ctermbg=white

" mark 100th column
set colorcolumn=128
highlight ColorColumn ctermbg=None ctermfg=None cterm=underline

" file saving
set backupcopy=yes
set noswapfile
set nowritebackup

" etc
set history=16

" searching
set hlsearch
set incsearch
set ignorecase
set smartcase

"
set autoindent

" tabs and deletion
set backspace=indent,eol,start
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=4

" keep this distance from top/bottom
set so=8

" fzf
let g:fzf_layout = { 'down': '~40%' }

" auto commands
set updatetime=512  " trigger after this many ms of inactivity
autocmd CursorHoldI,InsertLeave * write

let &t_SI.="\e[6 q" "SI = INSERT mode
let &t_SR.="\e[6 q" "SR = REPLACE mode
let &t_EI.="\e[6 q" "EI = NORMAL mode (ELSE)

" vim-markdown
let g:markdown_fenced_languages = ['cpp', 'rust', 'haskell', 'python', 'bash']

command Bat    execute "! clear && bat '%:p'"
command Build  execute "! clear && jvim-build '%:p'"
command TestBuild  execute "! clear && jvim-test-build '%:p'"
command BuildL execute "! clear && jvim-build-less '%:p'"
command Format execute "! clear && jvim-fmt '%:p'"
command Run    execute "! clear && jvim-run '%:p'"
command RunL   execute "! clear && jvim-run-less '%:p'"
command Test   execute "! clear && jvim-test  '%:p'"
command TestL  execute "! clear && jvim-test-less '%:p'"
command Copy   execute "! clear && pbcopy < %:p"
command Sk     execute "! sk ."
"alias broot='! clear && \broot . --hidden --show-git-info'

" keymaps
nnoremap ./    :Run<LF>
nnoremap .l    :RunL<LF>
nnoremap bb    :Build<LF>
nnoremap bl    :BuildL<LF>
nnoremap cc    :Copy<LF><LF>
nnoremap ff    :Format<LF>:edit!<LF><LF>
nnoremap fzb   :Buffers<LF>
nnoremap fzf   :Files<LF><LF>
nnoremap gd    :GitGutterPreviewHunk<LF><LF>
nnoremap gss   :GFiles?<LF><LF>
nnoremap gzf   :GFiles<LF><LF>
nnoremap lines :BLines<LF><LF>
nnoremap rg    :Rg 
nnoremap rr    :source $MYVIMRC<LF>:edit!<LF><LF>
nnoremap tb    :TestBuild<LF>
nnoremap tgs   :TagbarToggle<LF>
nnoremap tl    :TestL<LF>
nnoremap tt    :Test<LF>
nnoremap ww    :bd<LF>
